# Playground - Orchestrator
Front-end micro-service orchestrator for Playground

## Setup

### How do I spin up Orchestrator locally?
1. Clone this repository.
    ```PowerShell
    # Windows Powershell
    git clone git@gitlab.com:wayuki/playground/orchestrator.git
    cd .\orchestrator
    ```
    ```Shell
    # Unix Shell
    git clone git@gitlab.com:wayuki/playground/orchestrator.git
    cd ./orchestrator
    ```
2. Create a file `.env` with the content copied from `.env.example`, and fill out the values.
   1. `HOST` is the hostname of where the orchestrator will be served on, e.g. `0.0.0.0` or `local.wayuki.org`.
   2. `PUBLIC_URL` is a full URL which a browser can access the orchestrator, e.g. `http://localhost:3000`.
   3. `PORT` is the port which the orchestrator will be served in internally.

    <details>
      <summary markdown="span">Note</summary>
      Keep in mind that the port in `PORT` does not have to match the port in `PUBLIC_URL` if you are using Docker.

      For example, if you want the orchestrator to be served on port `3000` inside the Docker container, but want to map the port to `3333` on the host machine, the `PORT` should be `3000` while `PUBLIC_URL` should be `http://localhost:3333`.
    </details>
3. [Only when using Docker] Create a file `docker-compose.override.yml` with the content copied from `docker-compose.override.yml.example`, and customize it to your liking.
4. [Only when using Yarn on host] Run `yarn`.
5. Spin up Orchestrator.
    ```Shell
    # Use Docker
    docker-compose up local
    # Use NodeJS
    node start
    ```
6. Visit `PUBLIC_URL` on your browser

### What are the micro front-ends?
- [Foundation](https://gitlab.com/wayuki/playground/foundation), mainly serving navigation and error pages
- [Profile](https://gitlab.com/wayuki/playground/profile), serving profile and introductory pages

There are more to come.

FROM node:12.18-alpine

LABEL maintainer "Alfred See<adchuenhong@gmail.com>"

RUN apk update && \
    apk upgrade

WORKDIR /usr/src/app

COPY yarn.lock package.json ./

RUN yarn

COPY . /usr/src/app

CMD [ "./start" ]

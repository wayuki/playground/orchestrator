import history from './history';
import {
  getTokens, getUserInfo, getUserAccess, logout,
} from './auth';
import { setPageTitle, resetPageTitle } from './page-management';
import keys from './keys';

export const getStoreProps = async () => ({});
export const getServiceProps = async () => ({
  keys,
  auth: {
    getTokens,
    getUserInfo,
    getUserAccess,
    logout,
  },
  router: {
    history,
  },
  pageManagement: {
    setPageTitle,
    resetPageTitle,
  },
});

const webpack = require('webpack');
const fs = require('fs');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackBar = require('webpackbar');
const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const safePostCssParser = require('postcss-safe-parser');
const postcssNormalize = require('postcss-normalize');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const appDirectory = fs.realpathSync(process.cwd());
const ensureSlash = (inputPath, needsSlash) => {
  const hasSlash = inputPath.endsWith('/');
  if (hasSlash && !needsSlash) {
    return inputPath.substr(0, inputPath.length - 1);
  }

  if (!hasSlash && needsSlash) {
    return `${inputPath}/`;
  }

  return inputPath;
};

module.exports = (mode) => {
  const isDevelopment = mode === 'development';
  const isProduction = mode === 'production';

  let staticAssetPrefix = process.env.STATIC_ASSET_PREFIX || '';
  staticAssetPrefix = ensureSlash(staticAssetPrefix, staticAssetPrefix);

  const publicPath = ensureSlash(process.env.PUBLIC_URL || '/', true);
  const publicUrl = publicPath.slice(0, -1);

  const shouldUseRelativeAssetPaths = publicPath.startsWith('.');

  const env = Object.entries(process.env)
    .filter(([key]) => key.startsWith('PLAYGROUND_'))
    .reduce(
      (acc, [key, value]) => {
        acc[key] = value

        return acc
      },
      {
        NODE_ENV: process.env.NODE_ENV || 'development',
        PUBLIC_URL: publicUrl,
        REPORT_SERVICE_ENDPOINT: process.env.REPORT_SERVICE_ENDPOINT,
      },
    )

  return {
    mode: isProduction ? 'production' : isDevelopment && 'development',
    bail: isProduction,
    devtool: isProduction ? 'source-map' : isDevelopment && 'cheap-module-source-map',
    entry: [
      require.resolve('core-js/stable/index.js'),
      require.resolve('regenerator-runtime/runtime.js'),
      require.resolve('whatwg-fetch'),
      path.resolve(appDirectory, 'src/index.scss'),
      path.resolve(appDirectory, 'src/index.js'),
    ],
    output: {
      path: path.resolve(appDirectory, 'dist'),
      pathinfo: isDevelopment,
      filename: isProduction
        ? `${staticAssetPrefix}static/js/[name].[contenthash:8].js`
        : isDevelopment && `${staticAssetPrefix}static/js/[name].js`,
      futureEmitAssets: true,
      chunkFilename: isProduction
        ? `${staticAssetPrefix}static/js/[name].[contenthash:8].chunk.js`
        : isDevelopment && `${staticAssetPrefix}static/js/[name].chunk.js`,
      publicPath,
      devtoolModuleFilenameTemplate: isProduction
        ? (info) => path
          .relative(path.resolve(appDirectory, 'src'), info.absoluteResourcePath)
          .replace(/\\/g, '/')
        : isDevelopment && ((info) => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')),
    },
    optimization: {
      minimize: isProduction,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            parse: {
              ecma: 8,
            },
            compress: {
              ecma: 5,
              warnings: false,
              comparisons: false,
              inline: 2,
            },
            mangle: {
              safari10: true,
            },
            output: {
              ecma: 5,
              comments: false,
              ascii_only: true,
            },
          },
          sourceMap: true,
        }),
        new OptimizeCssAssetsPlugin({
          cssProcessorOptions: {
            parser: safePostCssParser,
            map: {
              inline: false,
              annotation: true,
            },
          },
          cssProcessorPluginOptions: {
            preset: ['default', { minifyFontValues: { removeQuotes: false } }],
          },
        }),
      ],
      splitChunks: {
        chunks: 'all',
        name: false,
      },
      runtimeChunk: {
        name: (entrypoint) => `runtime-${entrypoint.name}`,
      },
    },
    resolve: {
      modules: [
        'node_modules',
        path.resolve(appDirectory, 'node_modules'),
        path.resolve(appDirectory, 'src'),
      ],
      extensions: ['.js', '.jsx'],
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          parser: {
            requireEnsure: false,
          },
        },
        {
          test: /\.(js|mjs)$/,
          enforce: 'pre',
          include: path.resolve(appDirectory, 'src'),
          loader: require.resolve('eslint-loader'),
        },
        {
          oneOf: [
            {
              test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
              loader: require.resolve('url-loader'),
              options: {
                limit: 10000,
                name: `${staticAssetPrefix}static/media/[name].[hash:8].[ext]`,
              },
            },
            {
              test: /\.(js|mjs)$/,
              include: path.resolve(appDirectory, 'src'),
              loader: require.resolve('babel-loader'),
              options: {
                cacheDirectory: true,
                cacheCompression: false,
                compact: isProduction,
              },
            },
            {
              test: /\.css$/,
              use: [
                isDevelopment && require.resolve('style-loader'),
                isProduction && {
                  loader: MiniCssExtractPlugin.loader,
                  options: shouldUseRelativeAssetPaths ? { publicPath: '../../' } : {},
                },
                {
                  loader: require.resolve('css-loader'),
                  options: {
                    importLoaders: 1,
                    sourceMap: isProduction,
                  },
                },
                {
                  loader: require.resolve('postcss-loader'),
                  options: {
                    postcssOptions: {
                      ident: 'postcss',
                      plugins: [
                        require('postcss-flexbugs-fixes'),
                        require('postcss-preset-env')({
                          autoprefixer: {
                            flexbox: 'no-2009',
                          },
                          stage: 3,
                        }),
                        postcssNormalize(),
                      ],
                    },
                    sourceMap: isProduction,
                  },
                },
              ].filter(Boolean),
              sideEffects: true,
            },
            {
              test: /\.(scss|sass)$/,
              use: [
                isDevelopment && require.resolve('style-loader'),
                isProduction && {
                  loader: MiniCssExtractPlugin.loader,
                  options: shouldUseRelativeAssetPaths ? { publicPath: '../../' } : {},
                },
                {
                  loader: require.resolve('css-loader'),
                  options: {
                    importLoaders: 2,
                    sourceMap: isProduction,
                  },
                },
                {
                  loader: require.resolve('postcss-loader'),
                  options: {
                    postcssOptions: {
                      ident: 'postcss',
                      plugins: [
                        require('postcss-flexbugs-fixes'),
                        require('postcss-preset-env')({
                          autoprefixer: {
                            flexbox: 'no-2009',
                          },
                          stage: 3,
                        }),
                        postcssNormalize(),
                      ],
                    },
                    sourceMap: isProduction,
                  },
                },
                {
                  loader: require.resolve('resolve-url-loader'),
                  options: {
                    sourceMap: isProduction,
                  },
                },
                {
                  loader: require.resolve('sass-loader'),
                  options: {
                    sourceMap: isProduction,
                  },
                },
              ].filter(Boolean),
              sideEffects: true,
            },
            {
              test: /\.hbs$/,
              include: path.resolve(appDirectory, 'src'),
              loader: require.resolve('handlebars-loader'),
            },
            {
              loader: require.resolve('file-loader'),
              exclude: /\.(js|mjs|jsx|html|hbs|json)$/,
              options: {
                name: `${staticAssetPrefix}static/media/[name].[hash:8].[ext]`,
              },
            },
          ],
        },
      ],
    },
    plugins: [
      isDevelopment && new WebpackBar(),
      new HtmlWebpackPlugin(
        ({

          inject: true,
          template: path.resolve(appDirectory, 'src/index.hbs'),
          templateParameters: env,
          ...(isProduction
            ? {
              minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
              },
            }
            : undefined),
        }),
      ),
      isProduction && new MiniCssExtractPlugin({
        filename: `${staticAssetPrefix}static/css/[name].[contenthash:8].css`,
        chunkFilename: `${staticAssetPrefix}static/css/[name].[contenthash:8].chunk.css`,
      }),
      new webpack.DefinePlugin({
        'process.env': Object.entries(env)
          .reduce(
            (acc, [key, value]) => {
              acc[key] = JSON.stringify(value);

              return acc;
            },
            {},
          ),
      }),
      new ManifestPlugin({
        fileName: 'asset-manifest.json',
        publicPath,
        generate: (seed, files, entrypoints) => {
          const manifestFiles = files.reduce((manifest, file) => {
            manifest[file.name] = file.path;

            return manifest;
          }, seed);
          const entrypointFiles = entrypoints.main.filter(
            (fileName) => !fileName.endsWith('.map'),
          );

          return {
            files: manifestFiles,
            entrypoints: entrypointFiles,
          };
        },
      }),
    ].filter(Boolean),
    node: {
      module: 'empty',
      dgram: 'empty',
      dns: 'mock',
      fs: 'empty',
      http2: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    performance: false,
  };
};

/**
 * @typedef Service
 * @type {object}
 * @property {string} id - The ID of your service, must be unique
 * @property {string} baseUrl - The URL to the root directory of your service
 * @property {string} [manifestFileName="playground-manifest.json"]
 *  - The file name of the manifest listing your service's dependencies
 * @property {string} [moduleName="main"] - The name of the library in your Webpack output
 * @property {RegExp} pathRegex
 *  - The regular expression to determine whether the orchestrator should mount your service
 * @property {boolean} [required=false] - Whether the service must be mounted successfully
 * @property {('utility' | 'service')} [type="service"]
 *  - The type of the service. The orchestrator will ensure there is at least one "service" mounting
 *    or mounted at all time, whereas "utility" does not have this constraint.
 * @property {string | null} [storeId=null] - The ID of the store which belongs to the service
 */

/**
 * @typedef Store
 * @type {object}
 * @property {string} id - The ID of your store, must be unique
 * @property {string} baseUrl - The URL to the root directory of your service
 * @property {string} [manifestFileName="playground-manifest.json"]
 *  - The file name of the manifest listing your store's dependencies
 * @property {string} [moduleName="main"] - The name of the library in your Webpack output
 * @property {('instance' | 'factory')} [type="instance"]
 *  - The type of the store. If the type is "factory", the factory will be called with
 *    the store properties.
 * @property {string} exportName - The name of the exported store instance/factory
 */
